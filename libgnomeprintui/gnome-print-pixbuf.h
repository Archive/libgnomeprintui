/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

#ifndef __GNOME_PRINT_PIXBUF_H__
#define __GNOME_PRINT_PIXBUF_H__

/*
 *  gnome-print-pixbuf.h: A pixbuf print Context backend
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public License
 *  as published by the Free Software Foundation; either version 2 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Authors:
 *    Miguel de Icaza <miguel@ximian.com>
 *    Lauris Kaplinski <lauris@ximian.com>
 *
 *  Copyright (C) 1999-2002 Ximian Inc. and authors
 *
 */

#include <glib.h>

G_BEGIN_DECLS

#define GNOME_TYPE_PRINT_PIXBUF (gnome_print_pixbuf_get_type ())
#define GNOME_PRINT_PIXBUF(o)          (G_TYPE_CHECK_INSTANCE_CAST ((o), GNOME_TYPE_PRINT_PIXBUF, GnomePrintPixbuf))
#define GNOME_PRINT_PIXBUF_CLASS(k)    (G_TYPE_CHECK_CLASS_CAST    ((k), GNOME_TYPE_PRINT_PIXBUF, GnomePrintPixbufClass))
#define GNOME_IS_PRINT_PIXBUF(o)       (G_TYPE_CHECK_INSTANCE_TYPE ((o), GNOME_TYPE_PRINT_PIXBUF))
#define GNOME_IS_PRINT_PIXBUF_CLASS(k) (G_TYPE_CHECK_CLASS_TYPE    ((k), GNOME_TYPE_PRINT_PIXBUF))

typedef struct _GnomePrintPixbuf      GnomePrintPixbuf;
typedef struct _GnomePrintPixbufClass GnomePrintPixbufClass;

#include <libgnomeprint/gnome-print.h>

/*
 * The easiest way to get pixbufs is to connect signal handler to
 * "showpixbuf" signal of GnomePrintPixbuf object, with signature:
 *
 * void (* showpixbuf) (GnomePrintPixbuf *gpb, GdkPixbuf *pixbuf, guint pagenum);
 *
 * It will be called in each showpage, with copy of internal pixbuf, so you can
 * simply ref it, and use in your application. But beware that although same pixbuf
 * is not shared with context itself, it is shared by all signal handlers.
 */

GType gnome_print_pixbuf_get_type (void);

/* Creates a new GnomePrintPixbuf context */
GnomePrintContext * gnome_print_pixbuf_new (gdouble x0, gdouble y0,
								    gdouble x1, gdouble y1,
								    gdouble xdpi, gdouble ydpi,
								    gboolean alpha);

G_END_DECLS

#endif /* __GNOME_PRINT_PIXBUF_H__ */

