/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 *  gnome-print-pixbuf.c:
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public License
 *  as published by the Free Software Foundation; either version 2 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Authors:
 *    Miguel de Icaza <miguel@ximian.com>
 *    Lauris Kaplinski <lauris@ximian.com>
 *
 *  Copyright (C) 1999-2002 Ximian Inc. and authors
 *
 */

#include <config.h>
#include "gnome-print-pixbuf.h"

#include <math.h>
#include <stdio.h>
#include <libart_lgpl/art_affine.h>

#include <libgnomeprint/private/gnome-print-private.h>
#include <libgnomeprint/private/gnome-print-rbuf.h>

#include <libgnomeprintui/gnome-printui-marshal.h>

#include <gdk/gdkpixbuf.h>

enum {
	PROP_0 = 0,
	PROP_PIXBUF,
	PROP_PAGENUM
};

enum {
	SHOWPIXBUF,
	LAST_SIGNAL
};

typedef struct _GnomePrintPixbufPrivate	GnomePrintPixbufPrivate;

struct _GnomePrintPixbuf {
	GnomePrintRBuf rbuf;

	GnomePrintPixbufPrivate * private;
};

struct _GnomePrintPixbufClass {
	GnomePrintRBufClass parent_class;

	void (* showpixbuf) (GnomePrintPixbuf *gpb, GdkPixbuf *pixbuf, guint pagenum);
};

struct _GnomePrintPixbufPrivate {
	GdkPixbuf * pixbuf;

	gint pagenum;

	ArtDRect bbox;
};

static void gpix_class_init (GnomePrintPixbufClass * klass);
static void gpix_init (GnomePrintPixbuf * pixbuf);

static gint gpix_close (GnomePrintContext * pc);
static gint gpix_showpage (GnomePrintContext * pc);

static void gpix_private_clear_pixbuf (GnomePrintPixbuf * gpb);

static GnomePrintRBufClass * parent_class;
static guint gpix_signals[LAST_SIGNAL] = {0};

GType
gnome_print_pixbuf_get_type (void)
{
	static GType type = 0;

	if (!type){
		static const GTypeInfo info = {
			sizeof (GnomePrintPixbufClass), NULL, NULL,
			(GClassInitFunc) gpix_class_init, NULL, NULL,
			sizeof (GnomePrintPixbuf), 0, (GInstanceInitFunc) gpix_init
		};
		
		type = g_type_register_static (GNOME_TYPE_PRINT_RBUF, "GnomePrintPixbuf", &info, 0);
	}
	return type;
}

static void
gnome_print_pixbuf_finalize (GObject *object)
{
	GnomePrintPixbuf *p = GNOME_PRINT_PIXBUF (object);

	if (p->private) {
		if (p->private->pixbuf) {
			g_object_unref (G_OBJECT (p->private->pixbuf));
			p->private->pixbuf = NULL;
		}
		g_free (p->private);
		p->private = NULL;
	}

	G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
gnome_print_pixbuf_get_property (GObject *object, guint n,
		GValue *v, GParamSpec *pspec)
{
	GnomePrintPixbuf *p = GNOME_PRINT_PIXBUF (object);

	switch (n) {
	case PROP_PIXBUF:
		g_value_set_object (v, p->private->pixbuf);
		break;
	case PROP_PAGENUM:
		g_value_set_uint (v, p->private->pagenum);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, n, pspec);
		break;
	}
}

static gint
gnome_print_pixbuf_beginpage (GnomePrintContext *pc, const guchar *name)
{
	GnomePrintPixbuf *p = GNOME_PRINT_PIXBUF (pc);
	gint r;

	r = GNOME_PRINT_CONTEXT_CLASS (parent_class)->beginpage (pc, name);
	if (r != GNOME_PRINT_OK) return r;

	gnome_print_newpath (pc);
	gnome_print_moveto (pc, p->private->bbox.x0, p->private->bbox.y0);
	gnome_print_lineto (pc, p->private->bbox.x1, p->private->bbox.y0);
	gnome_print_lineto (pc, p->private->bbox.x1, p->private->bbox.y1);
	gnome_print_lineto (pc, p->private->bbox.x0, p->private->bbox.y1);
	gnome_print_closepath (pc);
	gnome_print_clip (pc);
	gnome_print_newpath (pc);

	return GNOME_PRINT_OK;
}

static void
gpix_class_init (GnomePrintPixbufClass * klass)
{
	GObjectClass * object_class;
	GnomePrintContextClass * print_class;

	object_class = (GObjectClass *) klass;
	print_class = (GnomePrintContextClass *) klass;

	parent_class = g_type_class_peek_parent (klass);

	gpix_signals[SHOWPIXBUF] = g_signal_new ("showpixbuf",
			G_TYPE_FROM_CLASS (object_class), G_SIGNAL_RUN_FIRST,
			G_STRUCT_OFFSET (GnomePrintPixbufClass, showpixbuf), NULL, NULL,
			libgnomeprintui_marshal_VOID__OBJECT_UINT, G_TYPE_NONE, 2,
			G_TYPE_OBJECT, G_TYPE_UINT);

	object_class->finalize     = gnome_print_pixbuf_finalize;
	object_class->get_property = gnome_print_pixbuf_get_property;

	print_class->beginpage = gnome_print_pixbuf_beginpage;
	print_class->showpage  = gpix_showpage;
	print_class->close     = gpix_close;

	g_object_class_install_property (object_class, PROP_PIXBUF,
			g_param_spec_object ("pixbuf", "Pixbuf", "Pixbuf", GDK_TYPE_PIXBUF,
				G_PARAM_READABLE));
	g_object_class_install_property (object_class, PROP_PAGENUM,
			g_param_spec_uint ("pagenum", "Page number", "Page number",
				0, G_MAXUINT, 0, G_PARAM_READABLE));
}

static void
gpix_init (GnomePrintPixbuf * pixbuf)
{
	pixbuf->private = g_new (GnomePrintPixbufPrivate, 1);

	pixbuf->private->pixbuf = NULL;
}

static int
gpix_close (GnomePrintContext *pc)
{
	GnomePrintPixbuf * gpb;
	GnomePrintPixbufPrivate * priv;
	gint ret;

	gpb = (GnomePrintPixbuf *) pc;
	priv = gpb->private;

	/* Destroyed object */
	g_return_val_if_fail (priv != NULL, -1);

	ret = 0;

	if (((GnomePrintContextClass *) parent_class)->close)
		ret = (* ((GnomePrintContextClass *) parent_class)->close) (pc);

	if (gpb->private->pixbuf) {
		g_object_unref (G_OBJECT (gpb->private->pixbuf));
		gpb->private->pixbuf = NULL;
	}

	return ret;
}

static int
gpix_showpage (GnomePrintContext * pc)
{
	GnomePrintPixbuf * gpb;
	GnomePrintPixbufPrivate * priv;
	GdkPixbuf * pixbuf;
	gint ret;

	gpb = (GnomePrintPixbuf *) pc;
	priv = gpb->private;

	/* Destroyed object */
	g_return_val_if_fail (priv != NULL, -1);

	/* Closed context */
	g_assert (priv->pixbuf != NULL);

	ret = 0;

	if (((GnomePrintContextClass *) parent_class)->showpage)
		ret = (* ((GnomePrintContextClass *) parent_class)->showpage) (pc);

	/* This silly - I have to modify RBuf, to accept new buffer, instead */
	pixbuf = gdk_pixbuf_copy (priv->pixbuf);
	g_signal_emit (G_OBJECT (pc), gpix_signals[SHOWPIXBUF], 0, pixbuf,
			priv->pagenum);
	g_object_unref (G_OBJECT (pixbuf));

	priv->pagenum++;

	gpix_private_clear_pixbuf (gpb);

	return ret;
}

static GnomePrintContext *
gnome_print_pixbuf_construct (GnomePrintPixbuf * gpb,
	gdouble x0, gdouble y0,
	gdouble x1, gdouble y1,
	gdouble xdpi, gdouble ydpi,
	gboolean alpha)
{
	GnomePrintPixbufPrivate * priv;
	gint width, height;
	gdouble translate[6], scale[6], page2buf[6];

	g_return_val_if_fail (gpb != NULL, NULL);
	g_return_val_if_fail (GNOME_IS_PRINT_PIXBUF (gpb), NULL);
	g_return_val_if_fail (x1 > x0, NULL);
	g_return_val_if_fail (y1 > y0, NULL);
	g_return_val_if_fail (xdpi > 0.0, NULL);
	g_return_val_if_fail (ydpi > 0.0, NULL);

	priv = gpb->private;

	g_assert (priv != NULL);

	priv->bbox.x0 = x0;
	priv->bbox.y0 = y0;
	priv->bbox.x1 = x1;
	priv->bbox.y1 = y1;

	priv->pagenum = 0;

	/* fixme: 72.0 or 72.some_fraction? */
	/* Rounde to next integer */
	/* fixme: We'll subtract a tiny value, to get over possible */
	/* fixme: rounding error if box really should be an integral */
	width = ceil ((x1 - x0) * xdpi / 72.0 - 1e-6);
	height = ceil ((y1 - y0) * ydpi / 72.0 - 1e-6);

	priv->pixbuf = gdk_pixbuf_new (GDK_COLORSPACE_RGB,
				       alpha, 8,
				       width,
				       height);
	
	g_return_val_if_fail (priv->pixbuf != NULL, NULL);

	gpix_private_clear_pixbuf (gpb);

	/* Buffer starts from top left corner */
	art_affine_translate (translate, -x0, -y1);

	/* Scaling */
	art_affine_scale (scale, xdpi / 72.0, -ydpi / 72.0);

	/* Composite page-to-buffer transformation */
	art_affine_multiply (page2buf, translate, scale);

	g_object_set (G_OBJECT (gpb),
			"pixels", gdk_pixbuf_get_pixels (gpb->private->pixbuf),
			"width", width,
			"height", height,
			"rowstride", gdk_pixbuf_get_rowstride (gpb->private->pixbuf),
			"page2buf", page2buf,
			"alpha", alpha, NULL);

	return GNOME_PRINT_CONTEXT (gpb);
}

GnomePrintContext *
gnome_print_pixbuf_new (gdouble x0, gdouble y0,
			gdouble x1, gdouble y1,
			gdouble xdpi, gdouble ydpi,
			gboolean alpha)
{
	GnomePrintPixbuf *gpb;

	g_return_val_if_fail (x1 > x0, NULL);
	g_return_val_if_fail (y1 > y0, NULL);
	g_return_val_if_fail (xdpi > 0.0, NULL);
	g_return_val_if_fail (ydpi > 0.0, NULL);

	gpb = g_object_new (GNOME_TYPE_PRINT_PIXBUF, NULL);

	if (!gnome_print_pixbuf_construct (gpb, x0, y0, x1, y1, xdpi, ydpi, alpha)) {
		g_object_unref (G_OBJECT (gpb));
		return NULL;
	}
	
	return GNOME_PRINT_CONTEXT (gpb);
}

/* Helpers */

static void
gpix_private_clear_pixbuf (GnomePrintPixbuf * gpb)
{
	GnomePrintPixbufPrivate * priv;
	gint x, y, width, height, rowstride;
	guchar * pixels, * p;
	gboolean alpha;

	g_assert (gpb != NULL);

	priv = gpb->private;

	g_assert (priv != NULL);
	g_assert (priv->pixbuf != NULL);

	width = gdk_pixbuf_get_width (priv->pixbuf);
	height = gdk_pixbuf_get_height (priv->pixbuf);
	rowstride = gdk_pixbuf_get_rowstride (priv->pixbuf);
	pixels = gdk_pixbuf_get_pixels (priv->pixbuf);
	alpha = gdk_pixbuf_get_has_alpha (priv->pixbuf);

	if (alpha) {
		for (y = 0; y < height; y++) {
			p = pixels + y * rowstride;
			for (x = 0; x < width; x++) {
				*p++ = 0xff;
				*p++ = 0xff;
				*p++ = 0xff;
				*p++ = 0x00;
			}
		}
	} else {
		for (y = 0; y < height; y++) {
			p = pixels + y * rowstride;
			for (x = 0; x < width; x++) {
				*p++ = 0xff;
				*p++ = 0xff;
				*p++ = 0xff;
			}
		}
	}
}
