#!/usr/bin/perl
#
# run-test.pl: runs a number of regresion tests for libgnomeprint
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, 
# Boston, MA 02111-1307, USA. 
#
# Author:
#    Chema Celorio <chema@ximian.com>
#

my $test_result = "output/test.result";

# Runs a command and returns is retval
# if @quiet = 1, does not emit the error on the console
# if $spacial case = 1, 1 is also a success return code, used by run_gpa_test
sub my_run_command
{
    my ($command, $quiet, $special_case) = (@_);
    my $ret = system ($command) / 256;

    if ($quiet ne 1) {
	 if ($special_case) {
	   if ($ret eq 99) {
		return $ret;
	   }
	 }
	   
	 if ($ret > 0) {
	   print "\n\n*** *** Error *** ***\n\n";
	   print "Command failed \"$command\"\nWith return code: " . $ret . "\n";
	   print "Error message:\n";
	   print "-------------------------------\n";
	   system ("cat " . $test_result);
	   print "-------------------------------\n\n";
	   print "run-test.pl unsucsessfull\n";
	 }
	 
    }
    
    return $ret;
}

#
# Calls ./gpa-test . composes the command line from the function args
#
sub run_gpa_test
{
    my ($sequence, $quiet) = (@_);
    my $command;
    
    $command  = "./gpa-test ";
    $command .= " --sequence=" . $sequence . " > " . $test_result . " 2>&1";
    
    my $ret = &my_run_command ($command, $quiet, 1);

    return $ret;
}


#
# Calls ./gpa-test . composes the command line from the function args
#
sub run_dialog_test
{
    my ($sequence, $quiet) = (@_);
    my $command;
    
    $command  = "./test-dialog ";
    $command .= " --num=" . $sequence . " > " . $test_result . " 2>&1";
    
    my $ret = &my_run_command ($command, $quiet, 1);

    return $ret;
}


#
# Calls ./gpa-test . composes the command line from the function args
#
sub my_check_preview
{
    my ($quiet) = (@_);
    my $command;

    print "\nTesting print preview ...";
    
    $command  = "./test-preview --kill ";
    
    my $ret = &my_run_command ($command, $quiet, 1);

    if ($ret ne 0) {
	   return $ret;
    }
    
    $command  = "GP_PREVIEW_STRICT_THEME=\"true\" ./test-preview --kill ";
    
    $ret = &my_run_command ($command, $quiet, 1);
    if ($ret ne 0) {
	   return $ret;
    }
    
    print "visual\n";
    
    return $ret;
}

#
# Test the gpaiu stuff
#
sub my_check_dialogs
{
    my $last = 0;
    my $i = 0;
    my $ret;
    my $test_list = "test_list.txt";

    print "\nTesting dialogs sequences ...\n";

    system ("./test-dialog --list-tests > " . $test_list);
    open (MY_FILE, $test_list);
    my @test_descriptions = <MY_FILE>;
    close (MY_FILE);
    
    while (not $last) {
	   chomp ($test_descriptions[$i]);
	   
	   print "  Testing dialog \#". $i . " [" . $test_descriptions[$i] . "]...";

	   $ret = run_dialog_test ($i, 0);

	   if ($ret eq 99) {
		  $last = 1;
		  $ret = 0;
	   }

	   if ($ret ne 0) {
		  print "Error.. command failed\n";
		  exit 1;
	   }
	 
	   print "pass\n";
	   $i ++;
    }
}			    

# Check that we can compile a gnome-print app, in this case we
# test generate.c
sub my_compile
{
    print "\nTesting compilation ...";

    my $command;

    if (0) {
	   $command  = "make clean ";
	   $command .= "> " . $test_result . " 2>&1 && ";
    }
    $command .= "make ";
    $command .= "> " . $test_result . " 2>&1";

    my $ret = &my_run_command ($command);
    if ($ret ne 0) {
	   print "failed\n";
	   exit 1;
    }

    print "pass\n";
    
    return $ret;
}

# We try running sutff that we know is good to check that the test is positive
# and tests known to fail to verify that the error is found.
sub my_sanity_check
{
    my $ret;

    print "\nRunning sanity check ...";
    
    print " pass\n";
}

# Checks the environment and dependencies
sub my_check_environment
{
    ## Empty ##
}

&my_check_environment ();
&my_compile ();
&my_sanity_check ();
&my_check_preview ();
&my_check_dialogs ();

print "\nrun-test.pl successful\n\n";

exit 0;
