-*- mode: m4 -*-
AC_PREREQ(2.52)
AC_INIT(libgnomeprintui, 2.18.7,
	http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-print)

AM_CONFIG_HEADER(config.h)
AM_INIT_AUTOMAKE(AC_PACKAGE_NAME, AC_PACKAGE_VERSION)

dnl make sure we keep ACLOCAL_FLAGS around for maintainer builds to work
AC_SUBST(ACLOCAL_AMFLAGS, "$ACLOCAL_FLAGS")

AM_MAINTAINER_MODE

AC_ISC_POSIX
AC_PROG_CC
AC_PROG_CPP
AC_PROG_CXX
AC_STDC_HEADERS
AC_LIBTOOL_WIN32_DLL
AM_PROG_LIBTOOL
IT_PROG_INTLTOOL([0.35.0])

# If the source code has changed at all, increment GNOMEPRINT_REVISION
# If any interfaces have been added, removed, or changed, increment GNOMEPRINT_CURRENT, and set GNOMEPRINT_REVISION to 0.
# If any interfaces have been added since the last public release, then increment GNOMEPRINT_AGE.
# If any interfaces have been removed since the last public release, then set GNOMEPRINT_AGE to 0.
GNOMEPRINT_REVISION=0
GNOMEPRINT_CURRENT=1
GNOMEPRINT_AGE=1
AC_SUBST(GNOMEPRINT_REVISION)
AC_SUBST(GNOMEPRINT_CURRENT)
AC_SUBST(GNOMEPRINT_AGE)

AC_DEFINE(WE_ARE_LIBGNOMEPRINT_INTERNALS, 1, [Define it so that we can read libgnomprint's private headers])

dnl =============
dnl Warning flags
dnl =============
GNOME_COMPILE_WARNINGS(maximum)
CFLAGS="$CFLAGS $warning_flags"

CFLAGS="$CFLAGS -DGDK_MULTIHEAD_SAFE"

dnl =======
dnl gettext
dnl =======
GETTEXT_PACKAGE=libgnomeprintui-2.2
AC_SUBST(GETTEXT_PACKAGE)
AC_DEFINE_UNQUOTED(GETTEXT_PACKAGE, "$GETTEXT_PACKAGE", [Gettext package])

AM_GLIB_GNU_GETTEXT

dnl ==========================================
dnl Checks for gtk-doc and docbook-tools
dnl ==========================================
GTK_DOC_CHECK([1.0])

dnl =================
dnl pkg-config checks
dnl =================
dnl It is ugly to put a build time check in for a run time dependency on gnome-icon-theme
dnl but this will hopefully keep the packagers on their toes
PKG_CHECK_MODULES(LIBGNOMEPRINTUI, [
	gtk+-2.0	   >= 2.6.0
	libgnomeprint-2.2  >= 2.12.1
	libgnomecanvas-2.0 >= 1.117.0
	gnome-icon-theme   >= 1.1.92
])
AC_SUBST(LIBGNOMEPRINTUI_CFLAGS)
AC_SUBST(LIBGNOMEPRINTUI_LIBS)

AC_PATH_PROG(GLIB_GENMARSHAL, glib-genmarshal)

dnl ================================================
dnl Used for the examples that need libglade support
dnl ================================================
PKG_CHECK_MODULES(LIBGLADE, [libglade-2.0], [libglade_msg=yes], [libglade_msg=no], [foo=yes])
AM_CONDITIONAL(WITH_LIBGLADE, test x"$libglade_msg" = "xyes")
AC_SUBST(LIBGLADE_CFLAGS)
AC_SUBST(LIBGLADE_LIBS)

dnl ============================================================================
dnl We might want to link the examples with 
dnl ../libgnomeprint/libgnomeprint/libgnomeprint-2-2.la to not have to make
dnl install after modifying libgnomeprint
dnl ============================================================================
if test "$cross_compiling" != yes; then
  AC_CHECK_FILE(../libgnomeprint/libgnomeprint/libgnomeprint-2-2.la, use_local_libgnomeprint_la="yes", use_local_libgnomeprint_la="no")
else
  use_local_libgnomeprint_la="no"
fi
AM_CONDITIONAL(EXAMPLES_USE_LOCAL_LIBGNOMEPRINT, test "x$use_local_libgnomeprint_la" = "xyes")

AC_OUTPUT([
Makefile
libgnomeprintui-zip
po/Makefile.in
libgnomeprintui/Makefile
libgnomeprintui/gpaui/Makefile
libgnomeprintui/libgnomeprintui-2.2.pc
libgnomeprintui/libgnomeprintui-uninstalled-2.2.pc
tests/Makefile
examples/Makefile
doc/Makefile
])

echo "
	Compiler:		${CC}
	Compiler flags:		${CFLAGS}"
if test "x$use_local_libgnomeprint_la" = "xyes" ; then
   echo "	Use local libgnomeprint-2-2.la for examples : Yes"
else
   echo "	Use local libgnomeprint-2-2.la for examples : No"
fi
if test "x$set_more_warnings" = "xno" ; then
   echo "	Disable more warnings:  Yes"
else
   echo "	Disable more warnings:  No"
fi
if test x$enable_gtk_doc = xyes ; then
   echo "	Enable gtk-doc:		Yes"
else
   echo "	Enable gtk-doc:		No"
fi
echo
