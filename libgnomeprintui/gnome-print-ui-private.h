/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */

#ifndef __GNOME_PRINT_UI_PRIVATE_H__
#define __GNOME_PRINT_UI_PRIVATE_H__

/*
 *  gnome-print-private.h: Private variables of the library
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public License
 *  as published by the Free Software Foundation; either version 2 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *  Authors:
 *    Ivan, Wong Yat Cheung <email@ivanwong.info>
 *
 *  Copyright (C) 2005 Novell Inc. and authors
 *
 */

extern gchar *gnome_printui_locale_dir;
extern gchar *gnome_printui_job_preview_data_dir;

#endif /*  __GNOME_PRINT_PRIVATE_H__ */
